#!/bin/bash

set -e

# настройки
project_name="py_dev_ib"
runner_dir="/home/gitlab-runner/builds/83bc9589/0/group.py_dev_ib/py_dev_ib/"

# Старт сервера с виртуальным окружением и из под обычного юзера

while getopts ":p :d" opt
do
    case "$opt" in
    p)
        env="prod"
    ;;
	d)
        env="dev"

    ;;
    *) echo "No reasonable options found!";;
    esac
done

project_dir="/srv/www/$project_name/$env/project"
virtualenv_dir="/srv/www/$project_name/$env/virtualenv"
# Папка где лежит settings.py
app_name='py_dev_ib'
python_version="3.5"

uid=server_user
gid=server_user

pidfile="$project_dir/tmp/uwsgi.pid"

if [ -f "$pidfile" ]
then
    echo '--- Завершаем uwsgi, не всегда срабатывает, проверяйте через htop'
    pid=$(cat "$pidfile")

    set +e
    kill -2 "$pid"
    set -e

    sleep 2
fi

cd "$project_dir"

source "$virtualenv_dir/bin/activate"

pip"$python_version" install --upgrade pip
cp -R $runner_dir* $project_dir
pip"$python_version" install -U -r requirements.txt
python"$python_version" manage.py migrate
python"$python_version" manage.py collectstatic --noinput --clear > /dev/null

# rm celerybeat.pid

# celery -A tasks worker --pool=solo -l info
# celery -A tasks beat

celery multi stopwait worker1 --pidfile="$project_dir/run/celery/%n.pid"
# rm $project_dir/run/celery/%n.pid
celery multi start worker1 \
    -A tasks \
    --pidfile="$project_dir/run/celery/%n.pid" \
    --logfile="$project_dir/logs/celery/%n%I.log"

deactivate

# chown -R "$uid":"$gid" "$project_dir"
# chown -R "$uid":"$gid" "$virtualenv_dir"

echo "--- Стартуем uwsgi"

/usr/bin/uwsgi \
    --chdir="$project_dir" \
    --module="$app_name".wsgi:application \
    --env DJANGO_SETTINGS_MODULE="$app_name".settings \
    --virtualenv="$virtualenv_dir" \
    --master \
    --pidfile="$pidfile" \
    --socket="$project_dir/tmp/uwsgi.sock" \
    --socket-timeout=300 \
    --processes=5 \
    --max-requests=500 \
    --vacuum \
    --daemonize="$project_dir/logs/uwsgi.log" \
    --disable-logging \
    --pcre-jit \
    --enable-threads \
    --uid="$uid" \
    --gid="$gid"

echo "--- Ok"

